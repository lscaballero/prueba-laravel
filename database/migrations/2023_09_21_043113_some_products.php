<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table('products')->insert([
            'name' => 'Apple Watch',
            'description' => 'El Apple Watch, es el primer reloj inteligente creado por la empresa Apple, presentado el 9 de septiembre de 2014 por Tim Cook.',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('products')->insert([
            'name' => 'Apple TV',
            'description' => 'Apple TV es un receptor digital multimedia diseñado, fabricado y distribuido por Apple Inc. El reproductor está diseñado para reproducir contenido multimedia digital.',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('products')->insert([
            'name' => 'Apple Pencil',
            'description' => 'El Apple Pencil es un periférico diseñado para Apple y concebido en un principio para funcionar con el iPad Pro.',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
