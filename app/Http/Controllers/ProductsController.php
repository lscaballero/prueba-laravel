<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\DAO\ProductoDAO;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    protected $productoDAO;

    public function __construct(ProductoDAO $productoDAO)
    {
        $this->productoDAO = $productoDAO;
    }

    public function index()
    {
        $productos = $this->productoDAO->obtenerTodosLosProductos();
        return response()->json($productos, 200);
    }

    public function show($id)
    {
        $producto = $this->productoDAO->obtenerProductoPorId($id);
        if (!$producto) {
            return response()->json(['message' => 'Producto no encontrado'], 404);
        }
        return response()->json($producto, 200);
    }

    public function store(Request $request)
    {
        // Validación de los datos del formulario
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        // Crear un nuevo producto
        $datosProducto = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ];

        $producto = $this->productoDAO->crearProducto($datosProducto);

        return response()->json(['message' => 'Producto creado con éxito', 'data' => $producto], 201);
    }

    public function update(Request $request, $id)
    {
        // Validación de los datos del formulario
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        // Actualizar un producto
        $datosProducto = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ];

        $producto = $this->productoDAO->actualizarProducto($id, $datosProducto);

        if (!$producto) {
            return response()->json(['message' => 'Producto no encontrado'], 404);
        }

        return response()->json(['message' => 'Producto actualizado con éxito', 'data' => $producto], 200);
    }

    public function destroy($id)
    {
        // Eliminar un producto
        $resultado = $this->productoDAO->eliminarProducto($id);

        if (!$resultado) {
            return response()->json(['message' => 'Producto no encontrado'], 404);
        }

        return response()->json(['message' => 'Producto eliminado con éxito'], 200);
    }

    public function listProducts()
    {
        $productos = $this->productoDAO->obtenerTodosLosProductos();

        return view('products', ['productos' => $productos]);
    }
}
