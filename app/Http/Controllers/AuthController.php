<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Autenticación exitosa, genera un token JWT
            $token = auth()->user()->createToken('NombreToken')->accessToken;

            // Redirige al usuario a la vista de lista de productos autorizada
            return redirect()->route('productos')->with('token', $token);
        } else {
            // Autenticación fallida
            return redirect('/login')->with('error', 'Credenciales inválidas');
        }
    }
}
