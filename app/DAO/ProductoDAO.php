<?php

namespace App\DAO;

use App\Models\Products;

class ProductoDAO
{
    public function obtenerTodosLosProductos()
    {
        return Products::all();
    }

    public function obtenerProductoPorId($id)
    {
        return Products::find($id);
    }

    public function crearProducto($datos)
    {
        return Products::create($datos);
    }

    public function actualizarProducto($id, $datos)
    {
        $producto = Products::find($id);
        if ($producto) {
            $producto->update($datos);
            return $producto;
        }
        return null;
    }

    public function eliminarProducto($id)
    {
        $producto = Products::find($id);
        if ($producto) {
            $producto->delete();
            return true;
        }
        return false;
    }
}
