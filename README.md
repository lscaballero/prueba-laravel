# Proyecto Laravel - Instrucciones de Despliegue

Este es un proyecto Laravel que requiere algunas configuraciones y pasos de despliegue antes de que pueda funcionar correctamente. Siga las instrucciones a continuación para configurar el proyecto en su entorno local.

## Requisitos previos

Antes de comenzar, asegúrese de tener lo siguiente instalado y configurado:

- **PostgreSQL:** Debe tener PostgreSQL instalado y configurado en su máquina. Cree una base de datos con el nombre "laravel" y un usuario "root" sin contraseña.

- **Composer:** Asegúrese de tener Composer instalado en su sistema.

## Pasos de despliegue

Siga estos pasos para configurar y ejecutar el proyecto:

1. **Descargue el proyecto:** Descargue el proyecto Laravel a su máquina.

2. **Directorio raíz:** Abra una terminal y navegue hasta el directorio raíz del proyecto.

3. **Instale las dependencias:** Ejecute el siguiente comando para instalar las dependencias del proyecto:

   ```bash
   composer install
   ```

4. Configuración de .env: Cree un archivo .env en la raíz del proyecto con la siguiente configuración:


```env
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:IFDC+5mWhKqk/eWDSxSS6p5jq7V2B/ngdvjLhLyh2ec=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=pgsql
DB_HOST=localhost
DB_PORT=5432
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailpit
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_HOST=
PUSHER_PORT=443
PUSHER_SCHEME=https
PUSHER_APP_CLUSTER=mt1

VITE_APP_NAME="${APP_NAME}"
VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
VITE_PUSHER_HOST="${PUSHER_HOST}"
VITE_PUSHER_PORT="${PUSHER_PORT}"
VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```


Asegúrese de configurar los valores específicos según su entorno.

5. Migraciones de base de datos: Ejecute las migraciones de la base de datos utilizando el siguiente comando:

    ```shell
    php artisan migrate

6. Ejecutar el siguiente comando para crear el usuario

    ```shell
    php artisan db:seed --class=UsersTableSeeder


6. Verificar la base de datos: Verifique que los datos se hayan migrado correctamente a la base de datos, especialmente la tabla "productos".


7. Iniciar el servidor de desarrollo: Inicie el servidor de desarrollo de Laravel ejecutando:

   ```shell
   php artisan serve


8. Verificación del funcionamiento: Puede verificar el funcionamiento del proyecto accediendo a través de la ruta /productos. También puede utilizar el archivo prueba-Laravel.postman_collection.json ubicado en la raíz del proyecto para realizar pruebas utilizando Postman.

¡Listo! Su proyecto Laravel ahora está configurado y en funcionamiento en su entorno local. Siéntase libre de explorar y desarrollar sobre esta base.


**Nota:** Para ver los productos hacer uso de los siguientes datos:

usuario: pruebas@solati.com
contraseña: prueba123

Este README.md presenta todos los pasos en una sola página para una mejor legibilidad. Asegúrese de que los detalles del archivo `.env` estén configurados correctamente según su entorno y requisitos específicos.
